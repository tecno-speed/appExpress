const express = require('express')
const app = express()

const cors = require('cors')
app.use(cors());

const bodyParser = require('body-parser')
app.use(bodyParser.json());

var mysql = require('mysql')
var connection = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'appexpress'
});

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/usuarios', (req, res) => {
    connection.query('SELECT * FROM contatos', function (err, rows, fields) {
        if (err) throw err
        res.json(rows);
    });
})

app.post('/usuarios', (req, res) => {
    var novoRegistro = req.body;
    var sql = 'INSERT INTO contatos (nome, email) VALUES (?, ?)';

    connection.query(sql, [novoRegistro.nome, novoRegistro.email], function (err, rows, fields) {
        if (err) throw err

        //insertId
        res.json(rows);
    });
})

app.put('/usuarios', (req, res) => {
    var novoRegistro = req.body;
    var sql = 'UPDATE contatos set nome = ?, email = ? WHERE (id = ?)';

    connection.query(sql, [novoRegistro.nome, novoRegistro.email, novoRegistro.id], function (err, rows, fields) {
        if (err) throw err
        res.json(rows);
    });
})

app.get('/usuarios/:id', (req, res) => {
    connection.query('SELECT * FROM contatos WHERE (id = ?)', [req.params.id], function (err, rows, fields) {
        if (err) throw err
        res.json(rows[0]);
    })
})

app.delete('/usuarios/:id', (req, res) => {
    connection.query('DELETE FROM contatos WHERE (id = ?)', [req.params.id], function (err, rows, fields) {
        if (err) throw err
        res.end('Registro removido');
    })
})

app.listen(3000, () => console.log('Rodando em 3000!'))